export class Weather {

    constructor(city, countryCode) {
        this.api = 'fbad30607d8a811e77dc8c8aedafa2d3'
        this.city = city;
        this.countryCode = countryCode;
    }

    async getWeather() {
        const URI = `https://api.openweathermap.org/data/2.5/weather?q=${this.city},${this.countryCode}&appid=${this.api}`;
    
        const res = await fetch(URI);

        const data = await res.json();

        return data;
    }

}